# python 3.x division
from __future__ import division

from django import template
from datetime import datetime
import time


register = template.Library()

@register.filter
def timestamp(value):
    try:
        return int(time.mktime(value.timetuple())*1000)
    except AttributeError:
        return None


@register.filter
def timestampsince(value):
    try:
        return int(time.mktime(datetime.now().timetuple())*1000) - int(time.mktime(value.timetuple())*1000)
    except AttributeError:
        return None


@register.assignment_tag
def timepercent(**kwargs):
    try:
        p = int(time.mktime(kwargs['p'].timetuple())*1000)
        n = int(time.mktime(datetime.now().timetuple())*1000)
        f = int(time.mktime(kwargs['f'].timetuple())*1000)

        if n > f: return 1
        if n < p: return 0
        
        return (n-p)/(f-p) 

    except AttributeError:
        return None


@register.assignment_tag
def confusion(**kwargs):
    seed = 9.4371034379073546
    time = kwargs['time']
    populations = kwargs['populations']

    return int( populations*(seed*pow(time,2)+1) )
