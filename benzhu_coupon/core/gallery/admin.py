from django.contrib import admin
from benzhu_coupon.core.gallery.models import *


class GalleryPicAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'admin_thumb', 'links')
    admin_thumb = AdminThumbnail(image_field='thumb')
    fields = ('name', 'pic', 'links')
    search_fields = ('id', 'name', 'links')


class GalleryAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    filter_horizontal = ('galleryPic',)
    search_fields = ('id', 'name')


#admin.site.register(GalleryUser, GalleryAdmin)
admin.site.register(GalleryManagement, GalleryAdmin)
#admin.site.register(GalleryUserPic, GalleryPicAdmin)
admin.site.register(GalleryManagementPic, GalleryPicAdmin)
