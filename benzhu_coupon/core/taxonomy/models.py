from django.db import models
from ckeditor.fields import RichTextField
from mptt.models import MPTTModel
from django.utils.translation import ugettext_lazy as _

'''
class Vocabulary(models.Model):
    name = models.CharField(_('Vocabulary Name'), max_length=100, blank=False)
    slug = models.SlugField(_('slug'), db_index=True)
    description = RichTextField(_('description'), default='')

    def __unicode__(self):  
        return u'%s' % self.name
'''


class Team(MPTTModel):
    #vocabulary = models.ForeignKey(Vocabulary)
    parent = models.ForeignKey('self', null=True, blank=True)
    name = models.CharField(_('Name'), max_length=100, blank=False)
    slug = models.SlugField(_('slug'), db_index=True)
    description = RichTextField(_('description'), default='')
    weight = models.IntegerField(_('weight'), )

    '''
    def __init__(self, *args, **kwargs):  
        super(Team, self).__init__(*args, **kwargs) 
        name = models.CharField('Team Name', max_length = 100, blank = False)
        name.contribute_to_class('name', self)
    '''

    class MPTTMeta:
        parent_attr = 'parent'

    class Meta:
        verbose_name = _('Team')
        verbose_name_plural = _('Team')

    def __unicode__(self):  
        return u'%s' % self.name  

