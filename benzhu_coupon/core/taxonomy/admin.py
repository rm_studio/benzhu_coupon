from django.db import models
from django.contrib import admin
from benzhu_coupon.core.taxonomy.models import *
from mptt.admin import MPTTModelAdmin


class TeamAdmin(MPTTModelAdmin):
    pass


admin.site.register(Team, TeamAdmin)
