from django.db import models
from ckeditor.fields import RichTextField
from benzhu_coupon.core.gallery.models import GalleryManagement
from benzhu_coupon.core.taxonomy.models import Team
from datetime import datetime
from django.utils.translation import ugettext_lazy as _


class Atom(models.Model):
    name = models.CharField(_('name'), max_length=150)
    gallery = models.ForeignKey(GalleryManagement, null=True, blank=True)
    info = RichTextField(_('info'), default='',  null=True, blank=True)
    time = models.DateTimeField(_('time'), default=datetime.now)
    state = models.CharField(_('state'), max_length=150, default='',  null=True, blank=True)
    taxonomy = models.ManyToManyField(Team, null=True, blank=True)

    class Meta:
        abstract = True
        verbose_name = _('Atom')
        verbose_name_plural = _('Atoms')