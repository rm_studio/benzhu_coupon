# settings/base.py

"""
Django settings for benzhu_coupon project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""


######################################################################

# Build paths inside the project like this: os.path.join(BASE_DIR, ...)

import os

from django.core.exceptions import ImproperlyConfigured

def get_env_variable(var_name):
    try:
        return os.environ[var_name]
    except KeyError:
        error_msg = "Set the %s environment variable" % var_name
        raise ImproperlyConfigured('error_msg')

BASE_DIR = os.path.dirname(os.path.dirname(__file__))
PROJECT_NAME = os.path.basename(BASE_DIR)

import sys  
  
reload(sys)  
sys.setdefaultencoding('utf8')

# Quick-start development settings - unsuitable for production
# See https://docs.djangoproject.com/en/1.7/howto/deployment/checklist/

# SECURITY WARNING: keep the secret key used in production secret!
# MUSE BE store into environment variable.
#   $ export SOME_SECRET_KEY=654-3jgwg-4r3-2t4h-76jk
#   $ export ANOTHER_SECRET_KEY=y5y-5jk8-75i5h-5g4/.-,o.
SECRET_KEY = 'k_^x2rn1h)aul_zyua5^11d=vm+0op__m)*-g#%!txp9b^96c*'

# Site ID
SITE_ID = 1

ALLOWED_HOSTS = ['*']


# Application definition

INSTALLED_APPS = (
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',
    'django.contrib.sites',
    'mptt',
    'ckeditor',
    'imagekit',
    'PIL',
    'benzhu_coupon',
    'benzhu_coupon.core.atom',
    'benzhu_coupon.core.gallery',
    'benzhu_coupon.core.taxonomy',
    'benzhu_coupon.apps.statistics',
    'benzhu_coupon.apps.unit',
    'benzhu_coupon.apps.company',
    'benzhu_coupon.apps.community',
    'benzhu_coupon.apps.coupon',
    'benzhu_coupon.apps.bzmember',
    'benzhu_coupon.apps.game.shake_it',
    'benzhu_coupon.apps.game.guest_it',
    'benzhu_coupon.apps.game.get_it',
    'benzhu_coupon.apps.game.luck_draw',
    'benzhu_coupon.apps.wechat.cardpack',
)

MIDDLEWARE_CLASSES = (
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.middleware.csrf.CsrfViewMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    #'django.middleware.cache.CacheMiddleware',
    'django.contrib.auth.middleware.SessionAuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',
    'django.middleware.clickjacking.XFrameOptionsMiddleware',
)

ROOT_URLCONF = PROJECT_NAME + '.urls'

# Publications.
WSGI_APPLICATION = PROJECT_NAME + '.wsgi.application'
#CACHE_BACKEND = 'memcached://127.0.0.1:11211/'
CACHE_BACKEND = 'locmem:///'


########################### Directories & URLs ############################

# Project path.
#PROJECT_DIR = os.path.join(BASE_DIR, PROJECT_NAME)
PROJECT_DIR = BASE_DIR 

# Static files (CSS, JavaScript, Images)
# https://docs.djangoproject.com/en/1.7/howto/static-files/
STATIC_ROOT = os.path.join(PROJECT_DIR, "static")
STATIC_URL = '/static/'

# Staticfiles directory IS NOT THE SAME AS Static directory
# Copy the statics files to staticfiles directory and empty the Static directory.
STATICFILES_DIRS = (
    # Put strings here, like "/home/html/static" or "C:/www/django/static".
    # Always use forward slashes, even on Windows.
    # Don't forget to use absolute paths, not relative paths.
    os.path.join(PROJECT_DIR, 'resource'),
    ("css", os.path.join(PROJECT_DIR, 'templates/default/css')),
    ("fonts", os.path.join(PROJECT_DIR, 'templates/default/fonts')),
    ("images", os.path.join(PROJECT_DIR, 'templates/default/images')),
    ("js", os.path.join(PROJECT_DIR, 'templates/default/js')),

    # Game's resource.
    # games share the same urls, so, cannot duplicate filename in each directory.
    #("game/images", os.path.join(PROJECT_DIR, 'apps/game/shake_it/templates/images')),
    #("game/images", os.path.join(PROJECT_DIR, 'apps/game/guest_it/templates/images')),
    #("game/images", os.path.join(PROJECT_DIR, 'apps/game/get_it/templates/images')),
    #("game/images", os.path.join(PROJECT_DIR, 'apps/game/luck_draw/templates/images')),
    ("game/images", os.path.join(PROJECT_DIR, 'resource/game/images')),

    #("game/sound", os.path.join(PROJECT_DIR, 'apps/game/shake_it/templates/sound')),
    #("game/sound", os.path.join(PROJECT_DIR, 'apps/game/guest_it/templates/sound')),
    ("game/sound", os.path.join(PROJECT_DIR, 'resource/game/sound')),

)

# Templates directory.
TEMPLATE_DIRS = (
    os.path.join(PROJECT_DIR, 'templates/default'),
)

# For Medias and django-ckeditor
MEDIA_URL = "/media/"
MEDIA_ROOT = os.path.join(PROJECT_DIR, "media")

CKEDITOR_UPLOAD_PATH = "ckeditor"
CKEDITOR_JQUERY_URL = '/static/common/libs/js/jquery/2.1.1/jquery.min.js'

# Upload & Thumbnail
UPLOAD_RELATE_ROOT = 'normal/'
THUMB_RELATE_ROOT = 'normal/thumb'
THUMB_URL = 'normal/thumb/'
THUMB_ROOT = os.path.join(MEDIA_ROOT, 'normal/thumb')

# locale
LOCALE_PATHS = (
    os.path.join(PROJECT_DIR, "conf/locale"),
)

# User
AUTH_USER_MODEL = 'bzmember.BzMember'

# User for @login_require
LOGIN_URL = '/'

###########################################################################

# CKEditor configs.

CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': (
        ['div', 'Source', '-', 'Save', 'NewPage', 'Preview', '-', 'Templates'],
        ['Cut', 'Copy', 'Paste', 'PasteText',  'PasteFromWord',  '-',  'Print',  'SpellChecker',  'Scayt'],
        ['Undo', 'Redo', '-', 'Find', 'Replace', '-', 'SelectAll', 'RemoveFormat'],
        ['Form', 'Checkbox', 'Radio', 'TextField', 'Textarea', 'Select', 'Button',  'ImageButton', 'HiddenField'],
        ['Bold', 'Italic', 'Underline', 'Strike', '-', 'Subscript', 'Superscript'],
        ['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', 'Blockquote'],
        ['JustifyLeft', 'JustifyCenter', 'JustifyRight', 'JustifyBlock'],
        ['Link', 'Unlink', 'Anchor'],
        ['Image', 'Flash', 'Table', 'HorizontalRule', 'Smiley', 'SpecialChar', 'PageBreak'],
        ['Styles', 'Format', 'Font', 'FontSize'],
        ['TextColor', 'BGColor'],
        ['Maximize', 'ShowBlocks', '-', 'About',  'pbckcode'],
        ),
    }
}
