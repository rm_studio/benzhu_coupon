# settings/base.py

"""
Django settings for benzhu_coupon project.

For more information on this file, see
https://docs.djangoproject.com/en/1.7/topics/settings/

For the full list of settings and their values, see
https://docs.djangoproject.com/en/1.7/ref/settings/
"""

from benzhu_coupon.conf.base import *
from benzhu_coupon.conf.local import *
