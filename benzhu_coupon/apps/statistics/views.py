#coding=utf-8

from cStringIO import StringIO
import os
import uuid, gzip, json
import urllib, urllib2 
from M2Crypto import BIO, RSA
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.template import loader, Context
from django.conf import settings 
from django.contrib.admin.views.decorators import staff_member_required


@staff_member_required
def tongji(request):

    # static variable
    username = u'\u62a2\u534a\u4ef7'
    uuid1 = str(uuid.uuid1())
    token = 'd77572a816173d0d991765fd2a64b6e2'
    account_type = '1'

    print [username, uuid1]

    preLoginHeader = {
            'UUID': uuid, 
            'account_type': account_type,
            'Content-Type': 'data/gzencode and rsa public encrypt;charset=UTF-8',
        }
    preLoginData = { 
            'username': username, 
            'token': token, 
            'functionName': 'preLogin', 
            'uuid': uuid1, 
            'request': { 
                'osVersion': 'MAC', 
                'deviceType': 'pc', 
                'clientVersion': '1.0', 
            }, 
        }
    DoLoginRequestImpl = {
            'username': username, 
            'token': token, 
            'functionName': 'doLogin', 
            'uuid': uuid1, 
            'request': { 
                'password': 'jmbenzhu2014', 
            }, 
        }

    # preLoginData,DoLoginRequestImpl: dict -> json
    jsonPreLoginData = json.dumps(preLoginData)
    jsonDoLoginRequestImpl = json.dumps(DoLoginRequestImpl)

    # compress jsonPreLoginData use gzip
    buf = StringIO()
    f = gzip.GzipFile(mode="wb", fileobj=buf)
    f.write(jsonPreLoginData)
    f.close()

    cjsonPreLoginData = buf.getvalue()

    buf = StringIO()
    f = gzip.GzipFile(mode="wb", fileobj=buf)
    f.write(jsonDoLoginRequestImpl)
    f.close()

    cjsonDoLoginRequestImpl = buf.getvalue()


    # encrypt compressed jsonPreLoginData with RSA public key.
    rsa = RSA.load_pub_key( os.path.join(getattr(settings, "PROJECT_DIR"), 'apps/statistics/apiPub.key') )
    postdata = ''
    postdologin = ''

    for i in range(0,len(cjsonPreLoginData)/117+1):
        postdata = postdata + rsa.public_encrypt(cjsonPreLoginData[117*i:117*(i+1)], RSA.pkcs1_padding)

    for i in range(0,len(cjsonDoLoginRequestImpl)/117+1):
        postdologin = postdologin + rsa.public_encrypt(cjsonDoLoginRequestImpl[117*i:117*(i+1)], RSA.pkcs1_padding)

    # url request
    url = 'https://api.baidu.com/sem/common/HolmesLoginService'
    request = urllib2.Request(url, postdata, headers=preLoginHeader)
    r = urllib2.urlopen(request)

    request = urllib2.Request(url, postdologin, headers=preLoginHeader)
    s = urllib2.urlopen(request)


    # extract request by gzip (excluding the first 8 bytes.)
    #   read the f**king man http://dev2.baidu.com/docs.do?product=4#page=Login
    buf = StringIO(r.read()[8:])
    f = gzip.GzipFile( fileobj=buf )
    f = f.read()

    buf = StringIO(s.read()[8:])
    g = gzip.GzipFile( fileobj=buf )
    g = g.read()

    print f
    print g

    doLoginResponse = json.loads(g)
    ucid = doLoginResponse['ucid']
    st = doLoginResponse['st']

    apiConnectionHeader = {
            'UUID': uuid1, 
            'USERID': ucid,
            'Content-Type': 'Content-Type:  data/json;charset=UTF-8',
        }
    parameterJSON = {
            "reportid": 1,
            "siteid": 1,
            "metrics": ["pageviews", "visitors", "ips", "exitRate"],
            "dimensions": ["pageid"],
            "start_time": "20150518000000",
            "end_time": "20150518235959",
            "filters": ["fromType=2", "newvisitor=1"],
            "start_index": 0,
            "max_results": 10000,
            "sort": ["pageviews desc", "exitRate desc"],
        }
    apiConnectionData = {
            'header': {
                'username': username,
                'password': st,
                'token': token,
                'account_type': account_type,
            },
            'body': {
                'serviceName': 'report',
                'methodName': 'query',
                'parameterJSON': json.dumps(parameterJSON),
            },
        }

    api_url = 'https://api.baidu.com/json/tongji/v1/ProductService/api'
    request = urllib2.Request(api_url, json.dumps(apiConnectionData).encode('utf-8'), headers=apiConnectionHeader)
    a = urllib2.urlopen(request)
    a = a.read()

    print json.dumps(apiConnectionData).encode('utf8')
    print a

    return HttpResponse(a)
