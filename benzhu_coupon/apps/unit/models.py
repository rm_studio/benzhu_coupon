from django.db import models
from benzhu_coupon.core.atom.models import Atom
from django.utils.translation import ugettext_lazy as _


class Unit(Atom):
    tel = models.CharField(_('tel'), max_length=150, default='', null=True, blank=True)
    address = models.CharField(_('address'), max_length=150, default='', null=True, blank=True)
    gps = models.CharField(_('gps'), max_length=50, default='', null=True, blank=True)

    class Meta:
        abstract = True
        verbose_name = _('Unit')
        verbose_name_plural = _('Units')