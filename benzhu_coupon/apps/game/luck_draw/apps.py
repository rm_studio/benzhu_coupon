from django.apps import AppConfig
from django.core import checks
from django.contrib.auth.checks import check_user_model

from django.utils.translation import ugettext_lazy as _


class AuthConfig(AppConfig):
    name = 'benzhu_coupon.apps.game.luck_draw'
    verbose_name = _("Games")

    def ready(self):
        checks.register(checks.Tags.models)(check_user_model)
