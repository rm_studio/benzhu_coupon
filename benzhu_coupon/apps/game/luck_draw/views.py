#coding:utf-8
from django.shortcuts import render

from django.template import loader, Context
from django.http import Http404, HttpResponse
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User  
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.contrib.admin.views.decorators import staff_member_required
from benzhu_coupon.apps.coupon.models import * 
from django.db.models import Q
import urllib, json


# Create your views here.
@login_required
def luck_draw(request):
    return render_to_response('luck_draw.html', 
            { }, context_instance=RequestContext(request))


@login_required
def get_ld_members(request, offset):
    # offset: coupon pk.
    way = Way.objects.get(url='luck_draw')
    coupon = Coupon.objects.get( Q(pk=offset) & Q(way=way) )
    way_logs = WayLog.objects.filter(coupon=coupon)

    print way_logs.count()
    for way_log in way_logs:
        print way_log.bz_member

    pass


@staff_member_required
def result_confirm(request, offset):
    # offset: coupon pk.
    try:
        way = Way.objects.get(url='luck_draw')
        coupon = Coupon.objects.get( Q(pk=offset) & Q(way=way) & Q(way_time__gt=timezone.now()) )
        way_logs = WayLog.objects.filter(Q(coupon=coupon) & Q(win_state=True))
    except Exception, e:
        return HttpResponse(e)

    # log for response
    log = ['Successful.',]

    for way_log in way_logs:
        if int(way_log.residue_count) != 0:
            # clear residue_count. 
            way_log.residue_count = 0 
            way_log.save()

            # get remain counts
            total_count = int(coupon.total_count)
            if any(coupon.received_count):
                received_count = int(coupon.received_count)
            else:
                received_count = 0 
            remain_counts = total_count - received_count  
            
            # is there any coupons here?
            if remain_counts > 0:
                # got it!
                # increase received count.
                coupon.received_count = received_count + 1
                coupon.save()

                # when executed, must reset win_state and residue_count.
                CouponLog().add_log(way_log.bz_member, offset)

                # log it
                log.append( (way_log.bz_member.username, way_log.bz_member.nickname) )

    return HttpResponse(json.dumps(log)) 


@login_required
def is_drawed(request, offset):
    try:
        # offset: coupon pk.
        way = Way.objects.get(url='luck_draw')
        coupon = Coupon.objects.get( Q(pk=offset) & Q(way=way) & Q(way_time__gt=timezone.now()) )
        print coupon.way_time
        way_logs = WayLog.objects.filter(Q(coupon=coupon) & Q(bz_member=request.user))

        if 0 < way_logs.count():
            return HttpResponse(True)
        else:
            return HttpResponse(False)
    except Exception:
        return HttpResponse(False)
