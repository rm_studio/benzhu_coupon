from django.contrib import admin
from benzhu_coupon.apps.coupon.models import Category, CouponType, Way, Coupon, WayLog, CouponLog
from django.contrib.admin import SimpleListFilter
from django.utils.translation import ugettext_lazy as _
from .models import LuckDraw


class LuckDrawAdmin(admin.ModelAdmin):
    list_display = ('get_id', 'get_name', 'get_operation' )

    def get_id(self, obj): return "%s" % (obj.pk)
    get_id.short_description = 'ID'
    
    def get_name(self, obj): return "%s" % (obj.name)
    get_name.short_description = _('Name')

    def get_operation(self, obj):
        return u"<a target='_blank' href='/game/luck_draw/result_confirm/%s/'>%s</a>" % ( obj.pk, _('Draw!') )
    get_operation.short_description = _('Operations')
    get_operation.allow_tags = True

    def get_queryset(self, request):
        return Coupon.objects.filter(way=Way.objects.filter(url='luck_draw')) 

    def has_add_permission(self, request):
        """
        Returns True if the given request has permission to add an object.
        Can be overridden by the user in subclasses.
        """
        return False


admin.site.register(LuckDraw, LuckDrawAdmin)
