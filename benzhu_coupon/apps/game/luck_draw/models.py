from django.db import models
from django.utils.translation import ugettext_lazy as _


class LuckDraw(models.Model):

    class Meta:
        verbose_name = _('Lucky Draw')
        verbose_name_plural = _('Lucky Draws')

    def __unicode__(self):
        return self.category_name
