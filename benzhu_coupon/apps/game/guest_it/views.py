#coding:utf-8
from django.shortcuts import render

from django.template import loader, Context
from django.http import Http404, HttpResponse
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User  
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.translation import ugettext_lazy as _
import urllib, json

# Create your views here.
def guest(request):
    return render_to_response('guest.html', 
            { }, context_instance=RequestContext(request))
