from benzhu_coupon.apps.coupon.models import *
from benzhu_coupon.core.gallery.models import *
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.utils import timezone
from datetime import datetime
from django.contrib.auth.decorators import login_required
from django.conf import settings
from benzhu_coupon.settings import *

import random, json, time


def all_coupons(request):
    # get settings from local
    WEIXIN_APPID = getattr(settings, "WEIXIN_APPID", "wxa4332df16d4d7a62")
    WEIXIN_REDIRECT_URI = getattr(settings, "WEIXIN_REDIRECT_URI", "http%3A%2F%2Fwww.bz088.com:8000/login")
    WEIXIN_RESPONSE_TYPE = getattr(settings, "WEIXIN_RESPONSE_TYPE", "code")
    WEIXIN_SCOPE = getattr(settings, "WEIXIN_SCOPE", "snsapi_login")
    WEIXIN_STATE = getattr(settings, "WEIXIN_STATE", "3d6be0a4035d839573b04816624a415e")
    WEIXIN_BASE_URL = getattr(settings, "WEIXIN_BASE_URL", "https://open.weixin.qq.com/connect/oauth2/authorize")
    WEIXIN_FORMAT_URL = getattr(settings, "WEIXIN_FORMAT_URL", "%s?appid=%s&redirect_uri=%s&response_type=%s&scope=%s&state=%s#wechat_redirect")

    # jump to some urls?
    if request.GET.get('next'):
        WEIXIN_REDIRECT_URI = WEIXIN_REDIRECT_URI + "?next=" + request.GET.get('next') 

    WEIXIN_AUTH_URL = WEIXIN_FORMAT_URL \
        % ( WEIXIN_BASE_URL, WEIXIN_APPID, WEIXIN_REDIRECT_URI, WEIXIN_RESPONSE_TYPE, WEIXIN_SCOPE, WEIXIN_STATE )

    # check user is authenticated ?
    if not request.user.is_authenticated():
        return HttpResponseRedirect( WEIXIN_AUTH_URL )

    # load coupons
    coupons = Coupon()
    r_coupons = coupons.get_ready_coupons()
    s_coupons = coupons.get_start_coupons()
    e_coupons = coupons.get_end_coupons()
    
    home_gallery = GalleryManagement.objects.get(pk=1)

    print("ready")
    print(r_coupons)
    print("start")
    print(s_coupons)
    print("end")
    print(e_coupons)
    return render_to_response('index.html', 
            {"home_gallery": home_gallery, "time": time.time(), "me": request.user.is_authenticated(), "r_coupons": r_coupons, "s_coupons": s_coupons, "e_coupons": e_coupons},
                 context_instance=RequestContext(request))


def search_coupons_by_taxonomy(request):
    key = request.GET.get('key')
    coupons = Coupon()
    r_coupons = coupons.search_ready_coupons_by_taxonomy(key)
    s_coupons = coupons.search_start_coupons_by_taxonomy(key)
    e_coupons = coupons.search_end_coupons_by_taxonomy(key)
    print("ready")
    print(r_coupons)
    print("start")
    print(s_coupons)
    print("end")
    print(e_coupons)
    if key == 'shake':
        return render_to_response('list_shake.html',
                    {"time": time.time(), "me": request.user.is_authenticated(), "r_coupons": r_coupons, "s_coupons": s_coupons, "e_coupons": e_coupons},
                     context_instance=RequestContext(request))
    if key == 'lottery':
        return render_to_response('list_lottery.html',
                    {"time": time.time(), "me": request.user.is_authenticated(), "r_coupons": r_coupons, "s_coupons": s_coupons, "e_coupons": e_coupons},
                     context_instance=RequestContext(request))
    if key == 'money':
        return render_to_response('list_get.html',
                    {"time": time.time(), "me": request.user.is_authenticated(), "r_coupons": r_coupons, "s_coupons": s_coupons, "e_coupons": e_coupons},
                     context_instance=RequestContext(request))
    return render_to_response('list_shake.html',
                    {"time": time.time(), "me": request.user.is_authenticated(), "r_coupons": r_coupons, "s_coupons": s_coupons, "e_coupons": e_coupons},
                     context_instance=RequestContext(request))


def search_coupons_by_way(request):
    key = request.GET.get('key')
    coupons = Coupon()
    r_coupons = coupons.search_ready_coupons_by_way(key)
    s_coupons = coupons.search_start_coupons_by_way(key)
    e_coupons = coupons.search_end_coupons_by_way(key)
    print("ready")
    print(r_coupons)
    print("start")
    print(s_coupons)
    print("end")
    print(e_coupons)
    return render_to_response('index.html',
                {"time": time.time(), "me": request.user.is_authenticated(), "r_coupons": r_coupons, "s_coupons": s_coupons, "e_coupons": e_coupons},
                 context_instance=RequestContext(request))



@login_required
def coupon_detail(request, offset):
    coupons = Coupon()
    coupon = coupons.get_coupon(offset)
    expire = None
    unstart = None
    over = None
    if coupon.activity_end_time < timezone.now():
        expire = "True"
    if coupon.activity_start_time > timezone.now():
        unstart = "True"
    if int(coupon.total_count) <= int(coupon.received_count):
        over = "True"
    if coupon.coupon_type.id == 4:
        return render_to_response('detail_money.html', {"time": time.time(), "coupon": coupon, "expire": expire, "unstart": unstart, "over": over}, context_instance=RequestContext(request))
    elif coupon.coupon_type.id == 5:
        return render_to_response('detail_lottery.html', {"time": time.time(), "coupon": coupon, "expire": expire, "unstart": unstart, "over": over}, context_instance=RequestContext(request))
    return render_to_response('detail.html', {"time": time.time(), "coupon": coupon, "expire": expire, "unstart": unstart, "over": over}, context_instance=RequestContext(request))


def coupon_company(request, offset):
    coupons = Coupon()
    coupon = coupons.get_coupon(offset)
    return render_to_response('company.html', {"company": coupon.company}, context_instance=RequestContext(request))


def coupon_win_list(request, offset):
    log = CouponLog()
    [coupon, coupon_logs] = log.get_win_list(offset)
    return render_to_response('win_list.html', {"coupon": coupon, "coupon_logs": coupon_logs}, context_instance=RequestContext(request))


def coupon_get(request, offset):
    log = CouponLog()
    log.add_log(request.user, offset)
    return render_to_response('center.html', {"member": request.user}, context_instance=RequestContext(request))


def coupon_fetch(request, offset):
    errors = []
    coupons = Coupon()
    coupon = coupons.get_coupon(offset)
    
    # get remain counts
    total_count = int(coupon.total_count)
    if any(coupon.received_count):
        received_count = int(coupon.received_count)
    else:
        received_count = 0 
    remain_counts = total_count - received_count  
   
    couponlog = CouponLog.objects.filter(bz_member=request.user, coupon=coupon)

    # get waylog obj or create it.
    try:
        waylog = WayLog.objects.get(bz_member=request.user, coupon=coupon)  
    except Exception:
        waylog = WayLog(
            bz_member=request.user,
            coupon=coupon,
            residue_count=coupon.way_count)
        waylog.save()

    waylog.last_way_time = timezone.now()
    waylog.save()

    if remain_counts <= 0: return HttpResponseRedirect('..#out-of-tickets')
    if int(waylog.residue_count) <= 0: return HttpResponseRedirect('..#play-over')
    if couponlog.count() >= int(coupon.max_count): return HttpResponseRedirect('..#get-over')

    if False not in [
        coupon.activity_start_time < timezone.now(), 
        coupon.activity_end_time > timezone.now(),
        ]:
        return HttpResponseRedirect('/game/'+coupon.way.url + '#' + str(coupon.id))
    else:
        return HttpResponseRedirect('..#error')


def coupon_go(request, offset):
    # common variables
    message = {
        'info': 'I lose it.',
        'triumph': False,
        'image': '',
        'times': 0,
    }

    # decrease user coupon remains.
    coupons = Coupon()
    coupon = coupons.get_coupon(offset)
   
    waylog = WayLog.objects.get(bz_member=request.user, coupon=coupon)  
    couponlog = CouponLog.objects.filter(bz_member=request.user, coupon=coupon) 

    if int(waylog.residue_count) != 0:
        # get random rate and generate it.
        triumph = random.random() < float(coupon.rate) and True or False 
        
        waylog.residue_count = int(waylog.residue_count) - 1 
        waylog.save()

        message['times'] = waylog.residue_count

        if triumph == True and \
                couponlog.count() <= int(coupon.max_count):
            # get remain counts
            total_count = int(coupon.total_count)
            if any(coupon.received_count):
                received_count = int(coupon.received_count)
            else:
                received_count = 0 
            remain_counts = total_count - received_count  
            
            # is there any coupons here?
            if remain_counts > 0:
                # got it!
                # increase received count.
                coupon.received_count = received_count + 1
                coupon.save()
                coupon_get( request, offset )        
                
                message['triumph'] = True
                message['info'] = 'I won this game.'
                message['title'] = coupon.name 
                message['image'] = coupon.company.gallery.galleryPic.first().pic.url
        else:
            message['triumph'] = False
    else:
        message['info'] = 'Numbers exceed.'

    return HttpResponse(json.dumps(message))


def all_my_coupons(member):
    log = CouponLog()
    coupon_logs = log.my_coupons(member)
    return coupon_logs


def search_my_coupons(request, offset):
    log = CouponLog()
    coupon_logs = log.search_my_coupons(request.user, offset)
    if offset == '1':
        return render_to_response('center_list_free.html', {"member": request.user, "coupon_logs": coupon_logs, "now": timezone.now()}, context_instance=RequestContext(request))
    elif offset == '2':
        return render_to_response('center_list_discount.html', {"member": request.user, "coupon_logs": coupon_logs, "now": timezone.now()}, context_instance=RequestContext(request))
    else:
        return render_to_response('center_list_money.html', {"member": request.user, "coupon_logs": coupon_logs, "now": timezone.now()}, context_instance=RequestContext(request))


def search_my_coupons_two_types(request, offset1, offset2):
    log = CouponLog()
    coupon_logs = log.search_my_coupons_two_types(request.user, offset1, offset2)
    return render_to_response('center.html', {"member": request.user, "coupon_logs": coupon_logs, "now": timezone.now()}, context_instance=RequestContext(request))


def all_my_lotteries(request, offset):
    log = CouponLog()
    coupon_logs = log.search_my_coupons(request.user, offset)
    log = WayLog()
    fail_lotteries = log.all_my_fail_lotteries(request.user)
    print(fail_lotteries)
    return render_to_response('center_list_lottery.html', {"member": request.user, "coupon_logs": coupon_logs, "fail_lotteries": fail_lotteries, "now": timezone.now()}, context_instance=RequestContext(request))


@login_required
def my_coupon_detail(request, offset):
    log = CouponLog()
    try:
        coupon_log = log.get_log_by_user(offset, request.user)
    except:
        return HttpResponseRedirect('/')

    expire = None
    if coupon_log.coupon.coupon_end_time < timezone.now():
        expire = "True"
    print(coupon_log)
    if coupon_log.coupon.coupon_type.id == 4:
        return render_to_response('my_coupon_money.html', {"coupon_log": coupon_log, "expire": expire}, context_instance=RequestContext(request))
    elif coupon_log.coupon.coupon_type.id == 5:
        return render_to_response('my_coupon_lottery.html', {"coupon_log": coupon_log, "expire": expire}, context_instance=RequestContext(request))
    return render_to_response('my_coupon.html', {"coupon_log": coupon_log, "expire": expire}, context_instance=RequestContext(request))


def consume(request):
    print("consume")
    coupon_log_id = request.GET.get('coupon_log_id')
    print(coupon_log_id)
    log = CouponLog()
    coupon_log = log.get_log(coupon_log_id)
    log.consume(coupon_log)
    return render_to_response('company_coupon.html', {"company": request.user.company, "coupon_log": coupon_log}, context_instance=RequestContext(request))


def company_coupon_detail(request, offset):
    coupons = Coupon()
    coupon = coupons.get_coupon(offset)
    expire = None
    if coupon.coupon_end_time < timezone.now():
        expire = "True"
    print(coupon)
    if coupon.coupon_type.id == 1 or coupon.coupon_type.id == 2:
        return render_to_response('company_coupon_detail.html', {"coupon": coupon, "expire": expire}, context_instance=RequestContext(request))
    if coupon.coupon_type.id == 4:
        return render_to_response('company_coupon_detail_money.html', {"coupon": coupon, "expire": expire}, context_instance=RequestContext(request))
    if coupon.coupon_type.id == 5:
        return render_to_response('company_coupon_detail_lottery.html', {"coupon": coupon, "expire": expire}, context_instance=RequestContext(request))
    #return render_to_response('company_coupon_detail.html', {"coupon": coupon, "expire": expire}, context_instance=RequestContext(request))


def company_coupon_win_list(request, offset):
    log = CouponLog()
    [coupon, coupon_logs] = log.get_win_list(offset)
    return render_to_response('company_win_list.html', {"coupon": coupon, "coupon_logs": coupon_logs}, context_instance=RequestContext(request))


def company_coupon_way_list(request, offset):
    log = CouponLog()
    [coupon, coupon_logs] = log.get_way_list(offset)
    return render_to_response('company_way_list.html', {"coupon": coupon, "coupon_logs": coupon_logs}, context_instance=RequestContext(request))


def coupon_check(request, offset):
    coupon = Coupon()
    t_coupon = coupon.get_coupon(offset)
    t_coupon.company_check = True
    t_coupon.company_check_time = datetime.now()
    t_coupon.save()
    expire = None
    if t_coupon.coupon_end_time < timezone.now():
        expire = "True"
    print(coupon)
    if t_coupon.coupon_type.id == 1 or t_coupon.coupon_type.id == 2:
        return render_to_response('company_coupon_detail.html', {"coupon": t_coupon, "expire": expire}, context_instance=RequestContext(request))
    if t_coupon.coupon_type.id == 4:
        return render_to_response('company_coupon_detail_money.html', {"coupon": t_coupon, "expire": expire}, context_instance=RequestContext(request))
    if t_coupon.coupon_type.id == 5:
        return render_to_response('company_coupon_detail_lottery.html', {"coupon": t_coupon, "expire": expire}, context_instance=RequestContext(request))
