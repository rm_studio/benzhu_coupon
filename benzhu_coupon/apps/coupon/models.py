# -*- coding: utf-8 -*-
from django.db import models
from benzhu_coupon.core.atom.models import *
from benzhu_coupon.apps.company.models import Company
from benzhu_coupon.apps.community.models import Community
from ckeditor.fields import RichTextField
from django.utils.translation import ugettext_lazy as _
from benzhu_coupon.apps.bzmember.models import BzMember
from django.db.models import Q
from django.utils import timezone

import random, uuid

class Category(models.Model):
    category_name = models.CharField(_('category_name'), max_length=150, default='', blank=True)
    parent = models.ForeignKey('self', default='', null=True, blank=True)

    class Meta:
        verbose_name = _('Category')
        verbose_name_plural = _('Categories')

    def __unicode__(self):
        return self.category_name


class CouponType(models.Model):
    type_name = models.CharField(_('type_name'), max_length=150, default='', blank=True)

    class Meta:
        verbose_name = _('CouponType')
        verbose_name_plural = _('CouponTypes')

    def __unicode__(self):
        return self.type_name


class Way(models.Model):
    way_name = models.CharField(_('way_name'), max_length=150, default='', blank=True)
    url = models.CharField(_('url'), max_length=150, default='', blank=True)

    class Meta:
        verbose_name = _('Way')
        verbose_name_plural = _('Ways')

    def __unicode__(self):
        return self.way_name


class Coupon(Atom):
    company = models.ForeignKey(Company, default='')
    #introduction = RichTextField(_('introduction'), default='')
    rule = RichTextField(_('rule'), default='')
    color_start = models.CharField(_('color_start'), max_length=150, default='#000000')
    color_end = models.CharField(_('color_end'), max_length=150, default='#000000')
    total_count = models.CharField(_('total_count'), max_length=150, default='')
    received_count = models.CharField(_('received_count'), max_length=150, default='0', null=True, blank=True)
    used_count = models.CharField(_('used_count'), max_length=150, default='0', null=True, blank=True)
    activity_online_time = models.DateTimeField(_('activity_online_time'), default=datetime.now)
    activity_start_time = models.DateTimeField(_('activity_start_time'), default=datetime.now)
    activity_end_time = models.DateTimeField(_('activity_end_time'), default='')
    #priority_index = models.IntegerField(_('priority_index'), max_length=11, default='0', null=True, blank=True)
    priority = models.IntegerField(_('priority'), max_length=11, default='0', null=True, blank=True)
    community = models.ManyToManyField(Community, default='', null=True, blank=True)
    category = models.ForeignKey(Category, default='')
    coupon_type = models.ForeignKey(CouponType, default='')
    original_price = models.CharField(_('original_price'), max_length=150, default='', null=True, blank=True)
    label = models.CharField(_('label'), max_length=150, default='')
    coupon_start_time = models.DateTimeField(_('coupon_start_time'), default=datetime.now)
    coupon_end_time = models.DateTimeField(_('coupon_end_time'), default='')
    required_points = models.CharField(_('required_points'), max_length=150, default='', null=True, blank=True)
    way = models.ForeignKey(Way, default='')
    rate = models.CharField(_('rate'), max_length=150, default='')
    # way_count: times for user playing games
    way_count = models.CharField(_('way_count'), max_length=150, default='1')
    # max_count: maximum counts for user getting coupons
    max_count = models.CharField(_('max_count'), max_length=150, default='1')
    way_time = models.DateTimeField(_('way_time'), null=True, blank=True)
    available = models.BooleanField(_('available'), default=True)
    company_check = models.BooleanField(_('company_check'), default=False)
    company_check_time = models.DateTimeField(_('company_check_time'), null=True, blank=True)

    class Meta:
        verbose_name = _('Coupon')
        verbose_name_plural = _('Coupons')

    def __unicode__(self):
        return self.name

    def get_ready_coupons(self):
        coupons = Coupon.objects.filter(Q(activity_online_time__lt=datetime.now()) & Q(activity_start_time__gt=datetime.now()) & Q(available="True")).order_by("activity_start_time", "-priority")
        return coupons

    def get_start_coupons(self):
        coupons = Coupon.objects.filter(Q(activity_start_time__lt=datetime.now()) & Q(activity_end_time__gt=datetime.now()) & Q(available="True")).order_by("activity_end_time", "-priority")
        return coupons

    def get_end_coupons(self):
        coupons = Coupon.objects.filter(Q(activity_end_time__lt=timezone.now()) & Q(available="True")).order_by("activity_end_time", "-priority")
        return coupons

    def search_ready_coupons_by_taxonomy(self, key):
        coupons = Coupon.objects.filter(Q(activity_online_time__lt=datetime.now()) & Q(activity_start_time__gt=datetime.now()) & Q(available="True") & Q(taxonomy__slug=key)).order_by("activity_start_time", "-priority")
        return coupons

    def search_start_coupons_by_taxonomy(self, key):
        coupons = Coupon.objects.filter(Q(activity_start_time__lt=datetime.now()) & Q(activity_end_time__gt=datetime.now()) & Q(available="True") & Q(taxonomy__slug=key)).order_by("activity_end_time", "-priority")
        return coupons

    def search_end_coupons_by_taxonomy(self, key):
        coupons = Coupon.objects.filter(Q(activity_end_time__lt=timezone.now()) & Q(available="True") & Q(taxonomy__slug=key)).order_by("-activity_end_time", "-priority")
        return coupons

    def search_ready_coupons_by_way(self, key):
        coupons = Coupon.objects.filter(Q(activity_online_time__lt=datetime.now()) & Q(activity_start_time__gt=datetime.now()) & Q(available="True") & Q(way__url=key)).order_by("activity_start_time", "-priority")
        return coupons

    def search_start_coupons_by_way(self, key):
        coupons = Coupon.objects.filter(Q(activity_start_time__lt=datetime.now()) & Q(activity_end_time__gt=datetime.now()) & Q(available="True") & Q(way__url=key)).order_by("activity_end_time", "-priority")
        return coupons

    def search_end_coupons_by_way(self, key):
        coupons = Coupon.objects.filter(Q(activity_end_time__lt=timezone.now()) & Q(available="True") & Q(way__url=key)).order_by("activity_end_time", "-priority")
        return coupons

    def get_coupon(self, coupon_id):
        coupon = Coupon.objects.get(id=coupon_id)
        return coupon

    def company_coupons(self, company):
        coupons = Coupon.objects.filter(company=company)
        return coupons


class WayLog(models.Model):
    bz_member = models.ForeignKey(BzMember, default='', blank=True)
    residue_count = models.CharField(_('residue_count'), max_length=150, default='', blank=True)
    coupon = models.ForeignKey(Coupon, default='', blank=True, related_name="CouponWayLog")
    last_way_time = models.DateTimeField(_('last_way_time'), null=True, blank=True)
    win_state = models.BooleanField(_('state'), default=False, blank=True)

    class Meta:
        verbose_name = _('WayLog')
        verbose_name_plural = _('WayLogs')

    def all_my_fail_lotteries(self, member):
        way_logs = WayLog.objects.filter(Q(bz_member=member) & Q(coupon__coupon_type__id=5) & Q(win_state=False)).order_by("-last_way_time")
        return way_logs


class CouponLog(models.Model):
    consume_code = models.CharField(_('consumer_code'), max_length=150, default='', blank=True)
    consume_state = models.CharField(_('consume_state'), max_length=150, default='', blank=True)
    bz_member = models.ForeignKey(BzMember, default='', blank=True)
    coupon = models.ForeignKey(Coupon, default='', blank=True)
    received_time = models.DateTimeField(_('received_time'), default=datetime.now)
    consume_date = models.DateTimeField(_('consume_date'), null=True, blank=True)

    class Meta:
        verbose_name = _('CouponLog')
        verbose_name_plural = _('CouponLogs')

    def all_my_coupons(self, member):
        coupon_logs = CouponLog.objects.filter(bz_member=member).order_by("-received_time")
        return coupon_logs

    def search_my_coupons(self, member, coupon_type_id):
        coupon_logs = CouponLog.objects.filter(Q(bz_member=member) & Q(coupon__coupon_type__id=coupon_type_id)).order_by("-received_time")
        return coupon_logs

    def search_my_coupons_two_types(self, member, coupon_type_id1, coupon_type_id2):
        coupon_logs = CouponLog.objects.filter(Q(bz_member=member) & (Q(coupon__coupon_type__id=coupon_type_id1) | Q(coupon__coupon_type__id=coupon_type_id2))).order_by("-received_time")
        return coupon_logs

    def get_log(self, log_id):
        log = CouponLog.objects.get(id=log_id)
        return log

    def get_log_by_user(self, log_id, member):
        log = CouponLog.objects.get(Q(id=log_id) & Q(bz_member=member))
        return log

    def get_log_by_consume_code(self, consume_code, company):
        log = CouponLog.objects.get(Q(consume_code=consume_code) & Q(coupon__company=company))
        return log

    def consume(self, log):
        print("original_state")
        print(log.consume_state)
        log.consume_state = "yes"
        log.consume_date = datetime.now()
        print("new_state")
        print(log.consume_state)
        log.save()
        coupon = log.coupon
        coupon.used_count = int(coupon.used_count) + 1
        coupon.save()
        return "success"

    def get_win_list(self, coupon_id):
        t_coupon = Coupon.objects.get(id=coupon_id)
        logs = CouponLog.objects.filter(coupon=t_coupon)
        return [t_coupon, logs]


    def get_way_list(self, coupon_id):
        t_coupon = Coupon.objects.get(id=coupon_id)
        logs = WayLog.objects.filter(coupon=t_coupon)
        return [t_coupon, logs]

    def add_log(self, member, coupon_id):
        log = CouponLog()
        coupon = Coupon.objects.get(id=coupon_id)
        
        # Method 1: generate random integer from range 0 to 9, 12 times.
        #log.consume_code = ''.join([str(random.randint(0,9)) for i in range(12)])
        
        # Method 2: crop 12-digits from uuid generator 1.
        log.consume_code = ''.join(random.sample(str(int(uuid.uuid1())), 12))
        
        log.consume_state = "not_yet"
        log.bz_member = member
        print(log.bz_member)
        log.coupon = coupon
        print(log.coupon)
        log.received_time = timezone.now()
        log.save()

