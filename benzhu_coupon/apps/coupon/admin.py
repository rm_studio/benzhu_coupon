from django.contrib import admin
from benzhu_coupon.apps.coupon.models import Category, CouponType, Way, Coupon, WayLog, CouponLog
from django.contrib.admin import SimpleListFilter
from django.utils.translation import ugettext_lazy as _


class CategoryAdmin(admin.ModelAdmin):
    list_display = ('id', 'category_name', 'parent')
    search_fields = ('id', 'category_name', 'parent')


class CouponTypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'type_name')
    search_fields = ('id', 'type_name')


class WayAdmin(admin.ModelAdmin):
    list_display = ('id', 'way_name', 'url')
    search_fields = ('id', 'way_name', 'url')


class CouponAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', "rate", 'coupon_type', 'priority', 'company', 'activity_online_time', 'activity_start_time',
                    'activity_end_time', 'coupon_end_time', 'way', "available", "company_check", 'total_count', 'received_count', 'used_count',)
    readonly_fields = ('received_count', 'used_count')
    filter_horizontal = ('taxonomy', 'community')
    search_fields = ('id', 'name', 'company__name', 'way__way_name', 'coupon_type__type_name')
    radio_fields = {"category": admin.HORIZONTAL, "coupon_type": admin.HORIZONTAL, "way": admin.HORIZONTAL}
    raw_id_fields = ('company', )
    list_editable = ('available', 'rate', 'activity_online_time', 'activity_start_time', 'activity_end_time', 'coupon_end_time', 'total_count')


class WayFilter(admin.SimpleListFilter):
    title = _('Way Filter')
    parameter_name = 'way_filter'
    
    def lookups(self, request, model_admin):
        ways = []
        for way in Way.objects.all():
            ways.append( (way.pk, way.way_name) )
        return tuple(ways) 
        
    def queryset(self, request, queryset):
        if self.value():
            return WayLog.objects.filter( 
                    coupon__in=Coupon.objects.filter(
                        way=Way.objects.get(pk=self.value()) )
            )


class WayLogAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_coupon', 'get_coupon_company', 'get_nickname', 'bz_member', 'residue_count', 'last_way_time', 'win_state')
    readonly_fields = ('id', 'coupon', 'bz_member', 'residue_count', 'last_way_time')
    search_fields = ('coupon__pk', 'coupon__name', 'coupon__company__name', 'bz_member__nickname')
    list_filter = (WayFilter,)
    list_editable = ('win_state',)

    def get_coupon(self, obj):
        return "[<a href='?q=%d'>%d</a>] %s" % (obj.coupon.pk, obj.coupon.pk, obj.coupon.name)
    get_coupon.short_description = 'Coupon'
    get_coupon.allow_tags = True

    def get_nickname(self, obj):
        return obj.bz_member.nickname
    get_nickname.short_description = 'nickname'

    def get_coupon_company(self, obj):
        return obj.coupon.company.name
    get_coupon_company.short_description = 'coupon_company'


class CouponLogAdmin(admin.ModelAdmin):
    list_display = ('id', 'get_coupon', 'get_coupon_company', 'get_nickname', 'bz_member', 'received_time', 'consume_date', 'consume_state')
    readonly_fields = ('id', 'coupon', 'bz_member', 'received_time', 'consume_date', 'consume_state')
    search_fields = ('coupon__pk', 'coupon__name', 'coupon__company__name', 'bz_member__nickname', 'consume_state')
    date_hierarchy = 'consume_date'
    list_filter = ('consume_state',)

    def get_coupon(self, obj):
        return "[<a href='?q=%d'>%d</a>] %s" % (obj.coupon.pk, obj.coupon.pk, obj.coupon.name)
    get_coupon.short_description = 'Coupon'
    get_coupon.allow_tags = True

    def get_nickname(self, obj):
        return obj.bz_member.nickname
    get_nickname.short_description = 'nickname'

    def get_coupon_company(self, obj):
        return obj.coupon.company.name
    get_coupon_company.short_description = 'coupon_company'


admin.site.register(Category, CategoryAdmin)
admin.site.register(CouponType, CouponTypeAdmin)
admin.site.register(Way, WayAdmin)
admin.site.register(Coupon, CouponAdmin)
admin.site.register(WayLog, WayLogAdmin)
admin.site.register(CouponLog, CouponLogAdmin)
