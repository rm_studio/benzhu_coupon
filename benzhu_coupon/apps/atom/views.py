from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.template import loader, Context
from benzhu_coupon.apps.coupon import views
from benzhu_coupon.apps.coupon.models import *
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.hashers import make_password
from django.utils import timezone
from django.contrib.auth.decorators import login_required


@login_required
def center(request):
    return render_to_response('center.html', {"member": request.user}, context_instance=RequestContext(request))


def about(request):
    t = loader.get_template("about.html")
    c = Context()
    return HttpResponse(t.render(c))


def about_us(request):
    t = loader.get_template("about_us.html")
    c = Context()
    return HttpResponse(t.render(c))


def safeguard(request):
    t = loader.get_template("safeguard.html")
    c = Context()
    return HttpResponse(t.render(c))


def company_login(request):
    account = None
    password = None
    error = None
    if request.method == 'POST':
        if not request.POST.get('account'):
            error = (_('please_input_account'))
            return render_to_response('company_login.html', {"error": error}, context_instance=RequestContext(request))
        if not request.POST.get('password'):
            error = (_('please_input_password'))
            return render_to_response('company_login.html', {"error": error}, context_instance=RequestContext(request))
        account = request.POST.get('account')
        password = request.POST.get('password')
        user = authenticate(username=account, password=password)
        if user is not None:
            if user.is_active:
                login(request, user)
                return HttpResponseRedirect('/company_center/')
            else:
                error = (_('disabled_account'))
                return render_to_response('company_login.html', {"error": error, "account": account}, context_instance=RequestContext(request))
        else:
            error = (_('login_error'))
            return render_to_response('company_login.html', {"error": error, "account": account}, context_instance=RequestContext(request))

    t = loader.get_template("company_login.html")
    c = Context()
    return render_to_response('company_login.html', {"account": account}, context_instance=RequestContext(request))


@login_required(login_url='/company_login')
def company_center(request):
    if request.method == 'POST':
        errors = []
        coupon_log = None
        expire = None
        over = None
        consume_code = request.POST.get('consume_code')
        log = CouponLog()
        try:
            coupon_log = log.get_log_by_consume_code(consume_code, request.user.company)
            if coupon_log.coupon.coupon_end_time < timezone.now():
                expire = "True"
            if int(coupon_log.coupon.total_count) <= int(coupon_log.coupon.used_count):
                over = "True"
            print(coupon_log.coupon)
            print(over)
        except:
            errors.append(_('no_coupon_log'))
        
        try:
            if coupon_log.coupon.coupon_type.id == 1 or coupon_log.coupon.coupon_type.id == 2:
                return render_to_response('company_coupon.html', {"coupon_log": coupon_log, "errors": errors, "consume_code": consume_code, "expire": expire, "over": over}, context_instance=RequestContext(request))
            if coupon_log.coupon.coupon_type.id == 4:
                return render_to_response('company_coupon_money.html', {"coupon_log": coupon_log, "errors": errors, "consume_code": consume_code, "expire": expire, "over": over}, context_instance=RequestContext(request))
            if coupon_log.coupon.coupon_type.id == 5:
                return render_to_response('company_coupon_lottery.html', {"coupon_log": coupon_log, "errors": errors, "consume_code": consume_code, "expire": expire, "over": over}, context_instance=RequestContext(request))
        except:
            return render_to_response('company_coupon.html', {"coupon_log": coupon_log, "errors": errors, "consume_code": consume_code, "expire": expire, "over": over}, context_instance=RequestContext(request))


    coupon = Coupon()
    coupons = coupon.company_coupons(request.user.company)
    return render_to_response('company_center.html', {"company": request.user.company, "coupons": coupons, "now": timezone.now()}, context_instance=RequestContext(request))


def company_setting(request):
    t = loader.get_template("company_setting.html")
    c = Context()
    return HttpResponse(t.render(c))


def company_logout(request):
    logout(request)
    return HttpResponseRedirect('/company_login')


def company_password(request):
    me = request.user
    if request.method == 'POST':
        error = None
        if not request.POST.get('password_org'):
            error = (_('please_input_password_org'))
            return render_to_response('company_password.html', {"error": error}, context_instance=RequestContext(request))
        if not request.POST.get('password'):
            error = (_('please_input_password_new'))
            return render_to_response('company_password.html', {"error": error}, context_instance=RequestContext(request))
        if not request.POST.get('password_again'):
            error = (_('please_input_password_again'))
            return render_to_response('company_password.html', {"error": error}, context_instance=RequestContext(request))
        password_org = request.POST.get('password_org')
        password = request.POST.get('password')
        password_again = request.POST.get('password_again')
        print(me.password)
        print(password_org)
        print(make_password(password_org))
        print(request.user.username)
        user = authenticate(username=request.user.username, password=password_org)
        print(user)
        if user is None:
            error = (_('password_org_error'))
            return render_to_response('company_password.html', {"error": error}, context_instance=RequestContext(request))
        if password != password_again:
            error = (_('different_passwords'))
            return render_to_response('company_password.html', {"error": error}, context_instance=RequestContext(request))
        me.password = make_password(password)
        me.save()
        error = (_('password_changed'))
        logout(request)
        return render_to_response('company_login.html', {"error": error}, context_instance=RequestContext(request))

    return render_to_response('company_password.html', {"me": me}, context_instance=RequestContext(request))


def stop(request):
    return render_to_response('stop.html', {}, context_instance=RequestContext(request))

