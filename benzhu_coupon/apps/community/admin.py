from django.contrib import admin
from benzhu_coupon.apps.community.models import Community


class CommunityAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'tel', 'state')
    filter_horizontal = ('taxonomy', )
    search_fields = ('id', 'name', 'tel', 'state')


admin.site.register(Community, CommunityAdmin)