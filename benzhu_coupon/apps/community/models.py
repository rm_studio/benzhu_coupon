from django.db import models
from benzhu_coupon.apps.unit.models import Unit
from django.utils.translation import ugettext_lazy as _


class Community(Unit):

    class Meta:
        verbose_name = _('Community')
        verbose_name_plural = _('Communities')

    def __unicode__(self):
        return self.name