from django.contrib.auth.models import AbstractUser
from django.db import models
from django.utils.translation import ugettext_lazy as _
from benzhu_coupon.apps.community.models import Community
from benzhu_coupon.apps.company.models import Company


class BzMember(AbstractUser):
    open_id = models.CharField(_('openid'), max_length=150, default='', null=True, blank=True)
    nickname = models.CharField(_('nickname'), max_length=150, default='', null=True, blank=True)
    sex = models.CharField(_('sex'), max_length=150, default='', null=True, blank=True)
    province = models.CharField(_('province'), max_length=150, default='', null=True, blank=True)
    city = models.CharField(_('city'), max_length=150, default='', null=True, blank=True)
    country = models.CharField(_('country'), max_length=150, default='', null=True, blank=True)
    head_image_url = models.CharField(_('head_image_url'), max_length=150, default='', null=True, blank=True)
    privilege = models.CharField(_('privilege'), max_length=150, default='', null=True, blank=True)
    union_id = models.CharField(_('union_id'), max_length=150, default='', null=True, blank=True)
    phone = models.CharField(_('phone'), max_length=150, default='', null=True, blank=True)
    points = models.CharField(_('points'), max_length=150, default='0', blank=True)
    community = models.ManyToManyField(Community, null=True, blank=True)
    company = models.ForeignKey(Company, null=True, blank=True)
    check = models.CharField(_('check'), max_length=150, default='', null=True, blank=True)

    class Meta(AbstractUser.Meta):
        swappable = 'AUTH_USER_MODEL'

    def __unicode__(self):
        return self.username

