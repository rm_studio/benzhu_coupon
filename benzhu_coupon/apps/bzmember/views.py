#coding:utf-8
from django.template import loader, Context
from django.http import Http404, HttpResponse
from django.http import HttpResponseRedirect
from django.contrib.auth.models import User  
from django.shortcuts import render_to_response
from django.template import RequestContext
from django.contrib.auth import authenticate, login as user_login, logout as user_logout
from django.utils.translation import ugettext_lazy as _
from django.conf import settings
from benzhu_coupon.settings import *

from .models import BzMember
import urllib2, json


def login(request):

    userinfo = []
    errors = []
    me = False
    if request.GET.get('code'):
        code = request.GET.get('code')
        
        appid = getattr(settings, "WEIXIN_APPID", "wxa4332df16d4d7a62")
        secret = getattr(settings, "WEIXIN_APP_SECRET", "e88886f15e648667597ff2e1584277d1")

        grant_type = 'authorization_code'

        url = 'https://api.weixin.qq.com/sns/oauth2/access_token' 
        pattern = '?appid=%s&secret=%s&code=%s&grant_type=%s' % ( appid, secret, code, grant_type )
        
        t = urllib2.urlopen(url+pattern)
        t = t.read()
        t = json.dumps(t)
        t = json.loads(t)
        access_token = eval(t)
        
        lang = 'zh_CN'

        url = 'https://api.weixin.qq.com/sns/userinfo' 
        try:
            pattern = '?access_token=%s&openid=%s&lang=%s' % ( access_token['access_token'], access_token['openid'], lang )  
        except:
            return HttpResponseRedirect('/about_us')

        t = urllib2.urlopen(url+pattern).read()
        t = json.dumps(t)
        t = json.loads(t)
        userinfo = eval(t)

        # try login
        account = userinfo['unionid']
        password = userinfo['unionid'] 

        user = authenticate(username=account, password=password)
        if user is not None:
            if not user.is_active:
                errors.append(_('disabled account'))
        else:
            # register
            email = account + '@bz088.com'
            
            user = BzMember.objects.create_user(account, email, password)
            user.is_active = True
            
            user.nickname = userinfo['nickname']

            user.sex = userinfo['sex']
            user.province = userinfo['province']
            user.city= userinfo['city']
            user.country = userinfo['country']
            user.union_id = userinfo['unionid']
            user.head_image_url = userinfo['headimgurl'].replace('\\','')
            
            try:
                user.save()
            except:
                user.nickname = _('unknown')
                user.save()

        user = authenticate(username=account, password=password)
        user_login(request, user)

        if request.GET.get('next'):
            return HttpResponseRedirect(request.GET.get('next'))
        else:
            return HttpResponseRedirect('/')
        

    if request.user.is_authenticated(): 
        me = True

    return render_to_response('login.html', 
            { 'userinfo': userinfo, 'me': me }, context_instance=RequestContext(request))


def logout(request):
    user_logout(request)
    return HttpResponseRedirect('/')

def fastlogin(request):
    # Direct login anonymous user for debuging.
    user = authenticate(username='anonymous', password='anonymous')
    
    if user is not None:
        user_login(request, user)
    else:
        user = BzMember.objects.create_user('anonymous', 'anonymous@no.where', 'anonymous')
        user.is_active = True
        user.save()
        user = authenticate(username='anonymous', password='anonymous')
        user_login(request, user)
    
    return HttpResponseRedirect('/')
