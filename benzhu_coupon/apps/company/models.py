from django.db import models
from benzhu_coupon.apps.unit.models import Unit
from django.utils.translation import ugettext_lazy as _
from ckeditor.fields import RichTextField


class Company(Unit):
    open_time = models.CharField(_('open_time'), max_length=150, default='', null=True, blank=True)
    detail_info = RichTextField(_('detail_info'), default='',  null=True, blank=True)
    priority = models.IntegerField(_('priority'), max_length=11, default='0', null=True, blank=True)
    color_start = models.CharField(_('color_start'), max_length=150, default='#000000')
    color_end = models.CharField(_('color_end'), max_length=150, default='#000000')
    available = models.BooleanField(_('available'), default=True)
    company_check = models.BooleanField(_('company_check'), default=False)
    company_check_time = models.DateTimeField(_('company_check_time'), null=True, blank=True)

    class Meta:
        verbose_name = _('Company')
        verbose_name_plural = _('Companies')

    def __unicode__(self):
        return self.name

    def get_companies(self):
        companies = Company.objects.filter(available="True")
        return companies

    def get_company(self, company_id):
        company = Company.objects.get(id=company_id)
        return company
