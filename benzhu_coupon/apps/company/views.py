from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.shortcuts import get_object_or_404, render_to_response
from django.template import RequestContext
from django.contrib.auth.decorators import login_required
from django.db.models import Q

from benzhu_coupon.apps.company.models import Company
from benzhu_coupon.apps.wechat.cardpack.models import WechatCompany, WechatCard, WechatLog
from benzhu_coupon.apps.wechat.cardpack.views import get_jsapi_sign

import time, hashlib


def company_info(request):
    return render_to_response('company_info.html', {"company": request.user.company, }, context_instance=RequestContext(request))


def info_list(request):
    company = Company()
    companies = company.get_companies()
    return render_to_response('info_list.html', {"companies": companies, }, context_instance=RequestContext(request))


@login_required
def info_detail(request, offset):
    company = Company()
    t_company = company.get_company(offset)
    card = None

    # wechat cards.
    try:
        wechat_company = WechatCompany.objects.get(company = t_company)
        cards = WechatCard.objects.filter(wechat_company = wechat_company)  
        print ( '***company_info_cards***', cards, )
    except WechatCompany.DoesNotExist:
        pass
    
    # if is a card_id included company info.
    if request.GET.get('card_id'):
        url_uuid = request.GET.get('card_id')
        card = get_object_or_404(WechatCard, url_uuid=url_uuid)
        card_obj = card
        print card

        # generate card signature.
        timestamp = int(time.time())

        #card.wechat_company.app_secret
        sign = [ card.card_id, card.wechat_company.get_api_ticket(), timestamp ]
        sign.sort()
        sign_str = ''.join(map(str,sign))
        card_signature = hashlib.sha1(sign_str).hexdigest()

        print sign

        card = { 
                'url_uuid': url_uuid,
                'card_id': card.card_id, 
                'card_name': card.title, 
                'card_description': card.description, 
                'company': card.custom_company.name, 
                'color': card.color, 
                'logo_url': card.logo_img, 
                'timestamp': timestamp, 
                'signature': card_signature, 
        }
        print ( '***card info***', card )

        jsapi_config = get_jsapi_sign(request)
        print jsapi_config
        
        wx_logs = WechatLog.objects.filter( Q(wechatcard=card_obj) & Q(action='add_card:ok') ).order_by('-action_time')
        print ( '***card received log***', wx_logs )

        for log in wx_logs: 
            print ( '***user logo***', log.user.head_image_url )
    
        return render_to_response('company_wx_card.html', 
                {'jsapi_config': jsapi_config, "company": t_company, 'card': card, 'logs': wx_logs, 'user': request.user, }, 
                context_instance=RequestContext(request))
    
    # is a normal company info.
    else:
        return render_to_response('company.html', {"company": t_company,}, context_instance=RequestContext(request))
