from django.contrib import admin
from benzhu_coupon.apps.company.models import Company


class CompanyAdmin(admin.ModelAdmin):
    list_display = ('id', 'name', 'tel', 'state')
    filter_horizontal = ('taxonomy', )
    search_fields = ('id', 'name', 'tel', 'state')


admin.site.register(Company, CompanyAdmin)