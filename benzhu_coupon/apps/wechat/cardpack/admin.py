from django.contrib import admin
from django import forms
from django.utils.translation import ugettext_lazy as _
from .models import WechatCompany, WechatCard, WechatLog


class WechatCompanyAdmin(admin.ModelAdmin):
    list_display = ['id', 'company', 'app_id', 'get_access_token', 'get_api_ticket' ]

    def get_access_token(self, obj):
        return obj.get_access_token()
    get_access_token.short_description = 'access_token'
    get_access_token.allow_tags = True

    def get_api_ticket(self, obj):
        return obj.get_api_ticket()
    get_api_ticket.short_description = 'api_ticket'
    get_api_ticket.allow_tags = True


class WechatCardAdmin(admin.ModelAdmin):
    add_form_template = 'change_form.html'
    change_form_template = 'change_form.html'

    list_display = ['id', 'title', 'description', 'card_id', 'wechat_company', 'custom_company', 'get_color', 'get_url', ]
    raw_id_fields = ['custom_company',]

    def get_color(self, obj):
        return u"<span style='background-color: %s; color: white'>&nbsp;%s&nbsp;</span>" % ( obj.color, obj.color )
    get_color.short_description = _('Color')
    get_color.allow_tags = True

    def get_url(self, obj):
        return u"URL: <a target='_Blank' href='/cardpack/get_card/?card_id=%s&wechat_card_js=1'>%s</a>" % ( obj.url_uuid,obj.url_uuid )
    get_url.short_description = _('UUID')
    get_url.allow_tags = True


class WechatLogAdmin(admin.ModelAdmin):
    list_display = [ 'id', 'user', 'wechatcard', 'action', 'action_time', ]


admin.site.register(WechatCompany, WechatCompanyAdmin)
admin.site.register(WechatCard, WechatCardAdmin)
admin.site.register(WechatLog, WechatLogAdmin)
