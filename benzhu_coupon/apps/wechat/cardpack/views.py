#coding=utf-8

from django.http import Http404, HttpResponse, HttpResponseRedirect
from django.template import loader, Context
from django.conf import settings 
from django.contrib.admin.views.decorators import staff_member_required
from django.shortcuts import get_object_or_404, render_to_response
from django.template import RequestContext
from django.core.cache import cache
from django.views.generic import View
from django.views.decorators.cache import cache_page

from .models import WechatCompany, WechatCard, WechatLog

import json
import random, time
import hashlib
import urllib2


def get_all_cards(request):
    cards = WechatCard.objects.all()
    return render_to_response('cardpack_staff.html', 
            { 'cards': cards }, context_instance=RequestContext(request))


def wx_write_log(request):
    action = request.GET.get('action')
    if action == 'add_card:ok':
        uuid = request.GET.get('uuid')
        log = WechatLog()
        log.user = request.user
        log.action = action
        log.wechatcard = get_object_or_404(WechatCard, url_uuid=uuid)
        log.save()
    pass


def get_jsapi_sign(request):
    # generate jsapi signature
    # static
    noncestr = 'Wm3WZYTPz0wzccnW'
    url = 'http://%s%s' % (request.get_host(), request.get_full_path())
    sign_str = 'jsapi_ticket=%s&noncestr=%s&timestamp=%d&url=%s'
    timestamp = int(time.time()) 

    # dynamic
    try:
        company = WechatCompany.objects.get(app_id='wxa6932e688742e714')
    except WechatCompany.DoesNotExist:
        # some error occur here. FIX IT.
        company = card.wechat_company

    sign_str = sign_str % ( company.get_api_ticket('jsapi'), noncestr, timestamp, url )
    jsapi_signature = hashlib.sha1(sign_str).hexdigest()

    print '*******************'
    print sign_str
    print jsapi_signature
    print '*******************'

    # get jsapi config info & signature.
    jsapi_config = { 'appid' : company.app_id, 'timestamp': timestamp, 'noncestr' : noncestr, 'signature': jsapi_signature, }

    return jsapi_config


class SimpleCardList(View):
    def get(self, request):
        company_id = int(request.GET.get('company')) 
        json_cards = get_object_or_404(WechatCompany, pk=company_id).batch_get_cards()
        return HttpResponse(json_cards)


class GetCard(View):
    def get(self, request):
        url_uuid = request.GET.get('card_id')
        card = get_object_or_404(WechatCard, url_uuid=url_uuid)
        print card

        # generate card signature.
        timestamp = int(time.time())

        #card.wechat_company.app_secret
        sign = [ card.card_id, card.wechat_company.get_api_ticket(), timestamp ]
        sign.sort()
        sign_str = ''.join(map(str,sign))
        card_signature = hashlib.sha1(sign_str).hexdigest()

        print sign

        # read card details.
        card_detail = urllib2.urlopen(
            data = json.dumps({ "card_id": card.card_id }),
            url = 'https://api.weixin.qq.com/card/get?access_token=%s' % card.wechat_company.get_access_token() )
        true, false = True, False
        card_info = eval(card_detail.read())

        print card_info        

        # generate jsapi signature
        jsapi_config = get_jsapi_sign(request)
        print jsapi_config

        card = { 
                'card_id': card.card_id, 
                'card_name': card.title, 
                'company': card.wechat_company.company.name, 
                'color': card.color, 
                'logo_url': card.logo_img, 
                'notice': card_info['card'][card_info['card']['card_type'].lower()]['base_info']['notice'], 
                'received': card_info['card'][card_info['card']['card_type'].lower()]['base_info']['sku']['total_quantity'] - 
                            card_info['card'][card_info['card']['card_type'].lower()]['base_info']['sku']['quantity'], 
                'timestamp': timestamp, 
                'signature': card_signature, 
        }
        return render_to_response('shake_cardpack.html', 
                { 'jsapi_config': jsapi_config, 'card': card }, context_instance=RequestContext(request))


def cardpack(request):

    #appid = 'wxa6932e688742e714'
    #secret = '6fd4aa277fd062f41737a2333fc81736'
    appid = 'wx4270e953fa77a5f7'
    secret = '5fc4c32284a81375fc7284421f9bf42e'

    timestamp = int(time.time())

    # get access token.
    # limited. MUST BE rewrite.
    credential = urllib2.urlopen('https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s' % (appid, secret) )
    access_token = eval( credential.read() )['access_token']

    # get api_ticket.
    # limited. MUST BE rewrite.
    api_ticket = urllib2.urlopen('https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=%s&type=jsapi' % access_token)
    api_ticket = eval(api_ticket.read())['ticket']

    print api_ticket

    # generate jsapi signature
    noncestr = 'Wm3WZYTPz0wzccnW'
    url = 'http://bz088.com:8001/cardpack/test/'
    sign_str = 'jsapi_ticket=%s&noncestr=%s&timestamp=%d&url=%s' % ( api_ticket, noncestr, timestamp, url )
    jsapi_signature = hashlib.sha1(sign_str).hexdigest()
    print sign_str
    print jsapi_signature

    # batch get cards.
    batchget_post = { "offset": "0", "count": "5" }
    f = urllib2.urlopen( data = json.dumps(batchget_post), url='https://api.weixin.qq.com/card/batchget?access_token=%s' % access_token )
    cards = eval( f.read() )

    random_card_index = int( random.random() * len(cards['card_id_list']))
    card_id = cards['card_id_list'][random_card_index]

    print card_id

    # read card details.
    card = urllib2.urlopen(
        data = json.dumps({ "card_id": card_id }),
        url = 'https://api.weixin.qq.com/card/get?access_token=%s' % access_token )

    card_info = card.read()

    # generate card signature.
    sign = [ card_id, secret, timestamp ]
    sign.sort()
    sign_str = ''.join(map(str,sign))
    card_signature = hashlib.sha1(sign_str).hexdigest()

    print card_signature

    # get jsapi config info & signature.
    jsapi_config = { 'appid' : appid, 'timestamp': timestamp, 'noncestr' : noncestr, 'signature': jsapi_signature, }

    # replace boolean variable for eval operation.
    true, false = True, False 
    card = { 'card_id': card_id, 'card_info': eval( card_info ), 'signature': card_signature, }

    print card['card_info']['card']['general_coupon']['base_info']['title']
    print jsapi_config

    return render_to_response('shake_cardpack.html', 
            { 'jsapi_config': jsapi_config, 'card': card }, context_instance=RequestContext(request))
