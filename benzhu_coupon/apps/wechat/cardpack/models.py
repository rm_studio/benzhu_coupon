from django.db import models
from benzhu_coupon.apps.company.models import Company
from benzhu_coupon.apps.bzmember.models import BzMember
from django.utils.translation import ugettext_lazy as _
from ckeditor.fields import RichTextField
from django.core.cache import cache
from django.utils import timezone

import urllib2, json, uuid


class WechatCompany(models.Model):
    company = models.ForeignKey(Company, null=False, blank=False)

    app_id = models.CharField(_('Application ID'), max_length=30, default='', null=False, blank=False, unique=True)
    app_secret = models.CharField(_('Application Secret'), max_length=50, default='', null=False, blank=False)

    class Meta:
        verbose_name = _('Wechat Company')
        verbose_name_plural = _('Wechat Companies')

    def __unicode__(self):
        return self.company.name

    def get_access_token(self):
        # get access token.
        # limited. catched.
        
        access_token_ideography = 'access_token_'+(self.app_id)
        
        if cache.get(access_token_ideography):
            return cache.get(access_token_ideography)
        else:
            url = 'https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=%s&secret=%s'
            credential = urllib2.urlopen( url % (self.app_id, self.app_secret) )
            credential = eval( credential.read() )
            print credential
            try:
                access_token = credential['access_token']
                cache.set(access_token_ideography, access_token, credential['expires_in'])
            except KeyError:
                return None

            return access_token

    def reset_access_token(self):
        # get wrong token, reset it.

        access_token_ideography = 'access_token_'+(self.app_id) 
        if cache.has_key(access_token_ideography):
            cache.delete(access_token_ideography)
            print 'reset_access_token'
            return get_access_token()

    def get_api_ticket(self, ticket_type='wx_card'):
        # get api_ticket.
        # limited. catched.
        
        api_ticket_ideography = 'api_ticket_'+ticket_type+'_'+(self.app_id)
        
        if cache.get(api_ticket_ideography):
            return cache.get(api_ticket_ideography)
        else:
            url = 'https://api.weixin.qq.com/cgi-bin/ticket/getticket?access_token=%s&type=%s' #jsapi, wx_card
            credential = urllib2.urlopen( url % (self.get_access_token(), ticket_type) )
            credential = eval( credential.read() )
            print credential
            try:
                api_ticket = credential['ticket']
                cache.set(api_ticket_ideography, api_ticket, credential['expires_in'])
            except KeyError:
                return None

            return api_ticket

    def batch_get_cards(self):
        # batch get cards.
        batchget_post = { "offset": "0", "count": "50", "status_list": ["CARD_STATUS_VERIFY_OK", "CARD_STATUS_DISPATCH"] }
        url = 'https://api.weixin.qq.com/card/batchget?access_token=%s'
        
        batch_get_cards_id = lambda act: urllib2.urlopen( data = json.dumps(batchget_post), url = url % act )

        f = batch_get_cards_id(self.get_access_token())
        cards = eval( f.read() )

        try:
            print cards['card_id_list']
        except KeyError:
            if cards['errcode'] == 40001:
                f = batch_get_cards_id(self.reset_access_token())
                cards = eval( f.read() )

        simple_cards = []
        for card_id in cards['card_id_list']:
            # read card details.
            card = urllib2.urlopen(
                data = json.dumps({ "card_id": card_id }),
                url = 'https://api.weixin.qq.com/card/get?access_token=%s' % self.get_access_token() )

            # replace boolean variable for eval operation.
            true, false = True, False 
            card_info = eval(card.read())

            simple_cards.append({   
                'company': self.id, 
                'original_company': self.company.pk,
                'card_id': card_id, 
                'color': card_info['card'][card_info['card']['card_type'].lower()]['base_info']['color'],
                'logo_url': card_info['card'][card_info['card']['card_type'].lower()]['base_info']['logo_url'],
                'title': card_info['card'][card_info['card']['card_type'].lower()]['base_info']['title'], 
                'description': card_info['card'][card_info['card']['card_type'].lower()]['base_info']['notice'], 
            })

        return json.dumps(simple_cards)


class WechatCard(models.Model):
    wechat_company = models.ForeignKey(WechatCompany, null=False, blank=False)
    title = models.CharField(_('Title'), max_length=255, default='', null=False, blank=False)
    custom_company = models.ForeignKey(Company, null=False, blank=False)
    description = models.CharField(_('Description'), max_length=2000, default='', null=True, blank=True)
    card_id = models.CharField(_('Card ID'), max_length=32, default='', null=False, blank=False)
    logo_img = models.CharField(_('Logo Images Url'), max_length=255, default='', null=False, blank=False)
    color = models.CharField(_('Color'), max_length=10, default='', null=False, blank=False)
    url_uuid = models.CharField(_('URL UUID'), max_length=100, default=hex(int(uuid.uuid1())), null=False, blank=False, unique=True)

    class Meta:
        verbose_name = _('Wechat Card')
        verbose_name_plural = _('Wechat Cards')

    def __unicode__(self):
        return self.title


class WechatLog(models.Model):
    user = models.ForeignKey(BzMember, null=True, blank=True) 
    wechatcard = models.ForeignKey(WechatCard, null=True, blank=True) 
    action = models.CharField(_('Action'), max_length=255, default='', null=True, blank=True)
    memo = models.CharField(_('Memo'), max_length=2000, default='', null=True, blank=True)
    action_time = models.DateTimeField(_('Action Time'), default=timezone.now)

    class Meta:
        verbose_name = _('Wechat Log')
        verbose_name_plural = _('Wechat Logs')
