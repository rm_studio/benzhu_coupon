from django.conf.urls import patterns, include, url
from django.conf.urls.static import static
from django.conf import settings
from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.contrib import admin
from benzhu_coupon.apps.statistics.views import *
from benzhu_coupon.apps.atom.views import *
from benzhu_coupon.apps.coupon.views import *
from benzhu_coupon.apps.bzmember.views import *
from benzhu_coupon.apps.company.views import *
from benzhu_coupon.apps.wechat.cardpack.views import *
from benzhu_coupon.apps.game.guest_it.views import guest 
from benzhu_coupon.apps.game.shake_it.views import shake 
from benzhu_coupon.apps.game.get_it.views import get 
from benzhu_coupon.apps.game.luck_draw.views import luck_draw, get_ld_members, is_drawed, result_confirm 

# urlpatterns.
urlpatterns = patterns('',
    url(r'^$', all_coupons),
    url(r'^search_by_taxonomy/$', search_coupons_by_taxonomy),
    url(r'^search_by_way/$', search_coupons_by_way),

    url(r'^stop/$', stop),

    url(r'^coupon/(\d+)/$', coupon_detail),
    url(r'^coupon/(\d+)/company$', coupon_company),
    url(r'^coupon/(\d+)/get/$', coupon_get),

    url(r'^coupon/(\d+)/fetch/$', coupon_fetch),
    url(r'^coupon/(\d+)/go/$', coupon_go),

    url(r'^info_list/$', info_list),
    url(r'^info_list/(\d+)/$', info_detail),
    
    url(r'^center', center),
    url(r'^my_coupon/(\d+)/$', my_coupon_detail),
    url(r'^my_coupon/type/(\d+)&(\d+)/$', search_my_coupons_two_types),
    url(r'^my_coupon/type/(\d+)/$', search_my_coupons),
    url(r'^my_lotteries/(\d+)/$', all_my_lotteries),
    url(r'^coupon/(\d+)/win_list/', coupon_win_list),
    url(r'^about/$', about),
    url(r'^about_us/', about_us),
    url(r'^safeguard/', safeguard),

    url(r'^company_login', company_login),
    url(r'^company_center/$', company_center),
    url(r'^company_center/(\d+)/$', company_coupon_detail),
    url(r'^company_center/(\d+)/win_list/$', company_coupon_win_list),
    url(r'^company_center/(\d+)/way_list/$', company_coupon_way_list),
    url(r'^company_center/(\d+)/check/$', coupon_check),
    url(r'^company_center/setting/$', company_setting),
    url(r'^company_center/consume/$', consume),
    url(r'^company_center/setting/change_password/$', company_password),
    url(r'^company_center/setting/company_info', company_info),
    url(r'^company_logout', company_logout),

    url(r'^list', list),
    url(r'^bzadmin/', include(admin.site.urls)),
    url(r'^ckeditor/', include('ckeditor.urls')),
    url(r'^login', login),
    url(r'^fl', fastlogin),
    url(r'^logout', logout),

    url(r'^game/guest', guest),
    url(r'^game/shake', shake),
    url(r'^game/get', get),
    url(r'^game/luck_draw$', luck_draw),
    
    url(r'^game/luck_draw/get_ld_members/(\d+)/$', get_ld_members ),
    url(r'^game/luck_draw/is_drawed/(\d+)/$', is_drawed ),
    url(r'^game/luck_draw/result_confirm/(\d+)/$', result_confirm),

    url(r'^tongji', tongji),
    url(r'^cardpack/test/$', cardpack),
    url(r'^cardpack/get_simple_card_list/$', SimpleCardList.as_view()),
    url(r'^cardpack/get_card/$', GetCard.as_view()),
    url(r'^cardpack/set_log/$', wx_write_log),
    url(r'^cardpack/get_all_cards/$', get_all_cards),
)

# Append
urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += staticfiles_urlpatterns()
