// Rin: contents sites all common javascript.

//<![CDATA[

// format date.
Date.prototype.format = function(format)  
{  
	/* 
	 * * format="yyyy-MM-dd hh:mm:ss"; 
	 * */  
	var o = {  
		"M+" : this.getMonth() + 1,  
		"d+" : this.getDate(),  
		"h+" : this.getHours(),  
		"m+" : this.getMinutes(),  
		"s+" : this.getSeconds(),  
		"q+" : Math.floor((this.getMonth() + 3) / 3),  
		"S"  : this.getMilliseconds()  
	}  
	if (/(y+)/.test(format)) {  
		format = format.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));  
	}  
	for (var k in o) {  
		if (new RegExp("(" + k + ")").test(format)) {  
			format = format.replace(RegExp.$1, RegExp.$1.length == 1  
				? o[k] : ("00" + o[k]).substr(("" + o[k]).length));  
	}}  

	return format;  
}  

// parse date ( for ios ) 
function parseDate(input, format) {
	format = format || 'yyyy-MM-dd- hh:mm'; // default format
	var parts = input.match(/(\d+)/g), i = 0, fmt = {};
	
	// extract date-part indexes from the format
	format.replace(/(yyyy|dd|MM|hh|mm)/g, function(part) { fmt[part] = i++; });
		  
	return new Date(
		parts[fmt['yyyy']], parts[fmt['MM']]-1, parts[fmt['dd']],
		parts[fmt['hh']],   parts[fmt['mm']] );
}

// string format
String.prototype.format = String.prototype.f = function() {
    var s = this, i = arguments.length;
    while (i--) { s = s.replace(new RegExp('\\{' + i + '\\}', 'gm'), arguments[i]); }
    
	return s;
};


/*********
 * Main
 */
$( function() {
	// home page.
	
	// fixed to top.
    /*
	origin_offsetTop = Array()
    	
	$('.container-fluid > .row > .coupon_state_label')
		.each( function() { origin_offsetTop.push( $(this).offset().top); } );

	$(window).scroll( function() {
		$('.container-fluid > .row > .coupon_state_label')
			.each( function(i,e) {
				if( $('body')[0].scrollTop >= $(this).offset().top - $('.blanking').height()  ) 
					$(this).css( { position: 'fixed', top: $('.blanking').height(), zIndex: 100 } );

				if( $('body')[0].scrollTop <= origin_offsetTop[i] + $('.blanking').height() )		
					$(this).css( { position: 'relative', top: 'auto' } );
		});
	});
	*/
	
	origin_offsetTop = Array()
    	
	$('.container-fluid > a[href="fetch"]')
		.each( function() { origin_offsetTop.push( $(this).offset().top); } );

	$(window).scroll( function() {
		$('.container-fluid > a[href="fetch"]')
			.each( function(i,e) {
				if( $('body')[0].scrollTop >= $(this).parent().offset().top ) 
					$(this).parent().css( { position: 'fixed', top: '0px', 
						paddingTop:'10px', marginLeft: 0, zIndex: 100, width: '100%', backgroundColor: 'white' } );

				if( $('body')[0].scrollTop <= origin_offsetTop[i] )		
					$(this).parent().css( { position: 'relative', top: 'auto', paddingTop:'0px', zIndex: 0 } );
		});
	});

	// Date Convert.
	end_time = Array();
	
	try {
		now = parseInt( (new Date()).valueOf() );  
		delay_time = now - local_time;
	}
	catch(e) { deviation = 0; }

	$('remain_time').each( function() {
		end_time.push(
			Date.parse(parseDate( $(this).text().replace(/[年月日]/ig,'-') )) 
		);
	});

	remain_counter = setInterval( function() {
		$('remain_time').each( function(i,e) {
			// when remain time exhausted, auto reload current page.
			if( 0 >= end_time[i] - ( (new Date()).valueOf() - deviation ) ) {
                clearTimeout( remain_counter );
                setTimeout(function () { window.location.reload() }, 1000);
            }
			
			Time = new Date( end_time[i] - ( (new Date()).valueOf() - deviation ) + 1000 );
			remain_time = "{0}:{1}:{2}"
				.f(Time.getUTCHours(), Time.getUTCMinutes(), Time.getUTCSeconds())
				.replace(/\b(\d)\b/ig,'0$1');

			$(this).text( remain_time ); 
		});
	}, 200 );

	// 3 in 1
	$('._3in1').each(function (i,e) {   
		$(this).bind( "input", { foo: i }, function(event) {
			var max_length = 4;
			if( $(this).val().length >= max_length ) {
				if(parseInt(event.data.foo) < 2) { 
					$('._3in1').eq(parseInt(event.data.foo)+1).focus();
				}
				else {
					$('input[type="submit"]').focus();
				}
				return false;
			}
		});

		$(this).bind( "click", function() {
			$(this).select();
		});	
	})
	.first().focus();
	
	$('[name="consume_code"]').hide();
	$('input[type="submit"]').bind( "click", function() {   
		$('[name="consume_code"]').val('');
		$('._3in1').each(function (i,e) { 
			$('[name="consume_code"]').val( $('[name="consume_code"]').val() + $(this).val() ) });
	} );

	// 1 in 3
	$('.consume_code consume_code').text(
		$('.consume_code consume_code').text().trim().replace(/(\d{4})(\d{4})(\d{4})/,'$1 $2 $3')
	);

	// nickname war.
	if( $('#nickname').text() == '' || $('#nickname').text() == 'unknown' ) {
		$('#nickname')
			.text('不能获取用户名')
			.bind('click', function() {
				if( window.confirm('你确定要投诉开发者吗？') ) alert('投诉没门 =。=！');
			});
	}

	// touch move effect.
	/*
	start_y = 0;
	document.addEventListener('touchmove',		
		function(evt) {  
			try  
			{  
				//evt.preventDefault();  
				var touch = evt.touches[0];
				var y = Number(touch.pageY);
				
				if( !Number( document.body.scrollTop ) ) { 
					if( !start_y ) start_y = y;
					else {
						$('.bg_center').css( "paddingTop", (y-start_y)/2 );
					}	
				}
				var text = 'TouchMove：（' +  ', ' + y + '）';  
				console.log( text);  
			}  
			catch (e) {  
				alert('touchMoveFunc：' + e.message);  
			}  
		}
	, false); 

	document.addEventListener('touchend', 
		function(evt) { $('.bg_center').css( "paddingTop", 0 ); }, false ); 
	*/	

	
	// footer_item menu: initialize.
	var active = function(obj) {
		try {
			var pressed = $(obj).find('img').attr('origin_src').replace(/\/(\w+).png/ig,'/$1_press.png'); 
		} 
		catch(e) {}

		$(obj).find('img').attr('src', pressed);
		$(obj).css( { color: '#86C12E' } );	
	};
	var deactive = function(obj) {
		$(obj).find('img').attr('src', $(obj).find('img').attr('origin_src'));
		$(obj).css( { color: 'black' } );	
	};
	var deactive_all = function(obj) {
		$(obj).each( function() { deactive(this)} );
	};

	// footer_item menu: touch hover.
	$('.footer_item').each( function() {
	    var origin  = $(this).find('img').attr('src');	
		$(this).find('img').attr('origin_src', origin);
		
		$(this).bind( 'touchstart', function() { active(this) } );
		$(this).bind( 'touchend', function() { deactive(this) } );
	} );

	// footer_item menu: activate.
	location.search.match(/\//ig) 
		? active($('.footer_item').first()[0]) : false;	
	location.search.match(/shake/ig) 
		? active($('.footer_item').eq(1)[0]) : false;	
	location.search.match(/lottery/ig) 
		? active($('.footer_item').eq(2)[0]) : false;	
	location.pathname.match(/(center)|(my_coupon)|(about_us)|(safeguard)|(my_lotteries)/ig) 
		? active($('.footer_item').eq(3)[0]) : false;	


	// logout
	$('#logout').bind( "click", function() {
		if( confirm("是否继续登出操作？") ) {
			$.ajax({
				type: "GET",
				url: "/logout",
				complete: function(msg) {
					WeixinJSBridge.invoke('closeWindow', {}, function(res){});
				},
			});
			
			// location = '/logout';
		}
	});	

});
//]]>
